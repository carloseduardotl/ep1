# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval)

## Informações

&nbsp;&nbsp;O jogo possui 3 tipos de Embarcações;

 * Canoa:
 
 Embarcação simples, ocupa uma posição e precisa somente de um ataque.
 
 * Submarino:
 
 Embarcação submersa, ocupa duas posições e é mais resistente.
 
 É destruida com 2 ataques em cada posição
 
 * Porta-aviões:
 
 Embarcação poderosa, ocupa 3 posições e é capaz de abater ataques.
 
 Cada ataque tem chance de não ser válido e ela precisa ser destruida nas 3 posições.
 

## Instruções

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

&nbsp;&nbsp;O jogo foi testado no terminal bash do Manjaro 18.

Criado por Carlos Eduardo Taborda Lotterman

Para a disciplina de Orientação a Objetos - FGA.

