#include "menu.hpp"
#include <iostream>
#include <unistd.h>

using namespace std;

Menu::Menu(){}

Menu::~Menu(){}

int Menu::get_opcao(){
	return opcao;
}

void Menu::set_opcao(int op){
	this->opcao = op;
}

void Menu::imprime_menu(){
	system("clear");
	int i;
	string lixo;
	
	cout << endl << endl << endl;
	cout << "  ____        _        _ _             _   _                  _ " << endl;
	cout << " | __ )  __ _| |_ __ _| | |__   __ _  | \\ | | __ ___   ____ _| |" << endl;
	cout << " |  _ \\ / _` | __/ _` | | '_ \\ / _` | |  \\| |/ _` \\ \\ / / _` | |" << endl;
	cout << " | |_) | (_| | |_ (_| | | | | | (_| | | |\\  | (_| |\\ V / (_| | |" << endl;
	cout << " |____/ \\__,_|\\__\\__,_|_|_| |_|\\__,_| |_| \\_|\\__,_| \\_/ \\__,_|_|" << endl;
	cout << "                                                                " << endl;
	cout <<  endl << endl << endl << endl;
	cout << "1 - Jogar criando Embarcações" << endl << "2 - Jogar importando Embarcações" << endl << "3 - Instruções" << endl << "4 - Sair" << endl;
	cout << endl << endl << "Digite uma opcao: ";
	cin >> i;
	set_opcao(i);
	if(i==3){
		system("clear");
		cout << endl << "O objetivo do jogo é destruir todos as Embarcações do seu campo de batalha." << endl;
		cout << "Existem 3 tipos de Embarcações:" << endl << endl;
		cout << "Canoas são as mais simples." << endl << "Ocupam apenas 1 posição e com um ataque são destruidas." << endl << endl;
		cout << "Submarinos são resistentes." << endl << "Eles precisam de 2 ataques para serem destruidos e ocupam 2 posições." << endl << endl;
		cout << "Porta-aviões podem abater seu ataque. Eles ocupam 3 posições." << endl << endl;
		cout << "A pontuação é dada pela diferença de Embarcações destruidas entre o vencedor e o perdedor." << endl << endl;
		cout << "Esse programa foi rodado através do terminal bash da distribuição Linux Manjaro 18" << endl;
		cout << "Digite qualquer coisa para voltar ";
		cin >> lixo;
		system("clear");
		imprime_menu();
	}
	if(i<1 || i>4){
		system("clear");
		cout << endl << endl << "	Digite uma opção válida!";
		imprime_menu();
	}
	
}
