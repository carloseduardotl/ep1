#include <iostream>
#include "embarcacao.hpp"
#include <string>

using namespace std;

Embarcacao::Embarcacao(){
	x=0;
	y=0;
	orientacao = "nada";
	destruido = false;
}

Embarcacao::~Embarcacao(){
}

int Embarcacao::get_x(){
	return x;
}

void Embarcacao::set_x(int x){
	this->x=x;
}

int Embarcacao::get_y(){
	return y;
}

void Embarcacao::set_y(int y){
	this->y=y;
}

string Embarcacao::get_orientacao(){
	return orientacao;
}

void Embarcacao::set_orientacao(string orientacao){
	this->orientacao=orientacao;
}

bool Embarcacao::get_destruido(){
	return destruido;
}

void Embarcacao::set_destruido(bool destruido){
	this->destruido=destruido;
}

