#include <iostream>
#include <string>
#include <fstream>
#include "jogo.hpp"
#include "menu.hpp"

using namespace std;

int main(){
	string nome;

	Jogo jogo1;
	Jogo jogo2;
	
	Menu menu;
	
	cout << "Jogador 1 insira seu nome: ";
	cin >> nome;
	jogo1.set_jogador(nome);

	cout << "Jogador 2 insira seu nome: ";
	cin >> nome;
	jogo2.set_jogador(nome);
	
	
	menu.imprime_menu();
	switch (menu.get_opcao()){
		case 1:
			jogo1.cria_mapa();
			jogo2.cria_mapa();
			break;		
		case 2:
			cout << "Função não implementada" << endl;
			return 0;
		case 4: 
			return 0;
	};
	
	
	do{
	jogo1.imprime_mapa();
	jogo1.tiro_jogador();
		if(jogo1.jogo_acabou()) break;
	
	jogo2.imprime_mapa();
	jogo2.tiro_jogador();
	}while (!jogo2.jogo_acabou());

	system("clear");
	
	if(jogo1.jogo_acabou()){
		cout << "Parabéns " << jogo1.get_jogador() << "! Voce ganhou o jogo!" << endl;
		cout << "Sua pontuação foi de " << jogo1.conta_pontos() - jogo2.conta_pontos() << " ponto(s)!" << endl; 
	}
	else{
		cout << "Parabéns " << jogo2.get_jogador() << "! Voce ganhou o jogo!" << endl;
		cout << "Sua pontuação foi de " << jogo2.conta_pontos() - jogo1.conta_pontos() << " ponto(s)!" << endl;
		}
	

    return 0;
}
