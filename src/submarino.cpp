#include "submarino.hpp"
#include <iostream>
#include <string>

Submarino::Submarino(int x, int y, std::string orientacao){
	set_x(x);
	set_y(y);
	set_orientacao(orientacao);
	set_quant_ataques(0);
}

Submarino::Submarino(){}
Submarino::~Submarino(){}

int Submarino::get_quant_ataques(){
	return quant_ataques;
}

void Submarino::set_quant_ataques(int quant){
	this->quant_ataques = quant;
}

