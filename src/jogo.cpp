#include "jogo.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

#include "embarcacao.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "porta_aviao.hpp"

using namespace std;

Jogo::Jogo(){}

Jogo::~Jogo(){}

string Jogo::get_jogador(){
	return jogador;
}

void Jogo::set_jogador(string jogador){
	this->jogador = jogador;
}

void Jogo::inicia_mapa(){
	int y, x;
	for(x=0; x<13; x++){
		for(y=0; y<13; y++){
			this->mapa[y][x]='~';
		}
	}
}

void Jogo::imprime_mapa(){
	int x, y;
	//cout << "	0 1 2 3 4 5 6 7 8 9 10 11 12"<< endl;
	system("clear");	
		for(x=0; x<13; x++){
			cout << x << "	";
			for(y=0; y<13; y++){
				cout << transforma_visivel(mapa[y][x]) << "  ";
			}
			cout << endl;
		}
		cout << "	0  1  2  3  4  5  6  7  8  9 10 11 12" << endl ;
}

void Jogo::atualiza_mapa(){
	
}

/*
Tabela:
~ = Água
X = Água já acertada
P = Porta aviões
p = Porta aviões destruido
S = Submarino
s = Submarino com 1 ataque
0 = Submarino destruido
C = Canoa
c = Canoa destruida
*/


void Jogo::tiro(int x, int y){
	switch(mapa[y][x]){
		case '~':
			cout << "Você acertou na água!" << endl;
			mapa[y][x] = 'X';
			break;
		case 'X':
			cout << "Você já atirou aqui antes, tenha mais atenção por favor!" << endl;
			break;
		case 'P':
			if (tiro_porta_aviao()){
				cout << "Você acertou parte do Porta-aviões" << endl;
				mapa[y][x] = 'p';
			}
			else
				cout << "Poxa! Um Porta-aviões abateu seu tiro :( Tente novamente" << endl;
			break;
		case 'p':
			cout << "Essa parte do Porta-aviões já foi destruida" << endl;
			break;
		case 'S':
			cout << "Você acertou parte do Submarino, porém ele continua operante." << endl << "Tente mais um ataque" << endl;
			mapa[y][x] = 's';
			break;
		case 's':
			cout << "Você destruiu parte do Submarino! Parabéns!" << endl;
			mapa[y][x] = '0';
			break;
		case '0':
			cout << "Essa parte do Submarino já foi destruida" << endl;
			break;
		case 'C':
			cout << "Você destruiu uma Canoa. Parabéns!" << endl;
			mapa[y][x] = 'c';
			break;
		case 'c' :
			cout << "Essa Canoa ja foi destruida, tente outra posicao" << endl;
			break;		
	}
	system("sleep 2");
}

bool Jogo::tiro_porta_aviao(){
	srand((unsigned)time(0)); //gerar números aleatórios entre 0 e 1
      
    int aleatorio = rand()%(2);
    if(aleatorio == 1) return true;
    else return false;	
}

void Jogo::teste_mapa(){
	mapa[7][1]='P';
}

void Jogo::tiro_jogador(){
	int x, y;
	
	cout << get_jogador() << ", digite a posição da linha: ";
	cin >> x;
	cout << endl << get_jogador() << ", digite a posição da coluna: ";
	cin >> y;
	if((x>=13 || y>=13) || (x<0 || y<0)){
		cout << "Digite uma posicão válida. Entre 0 e 12" << endl << endl;
		tiro_jogador();
		}
	
	tiro(x, y);
	system("sleep 1");
}

bool Jogo::jogo_acabou(){
	int x, y;
	for(x=0; x<13; x++)
		for(y=0; y<13; y++){
			if(mapa[x][y]=='C' || mapa[x][y]=='s' || mapa[x][y]=='S' || mapa[x][y]=='P')
				return false;
		}
		return true;
}

void Jogo::cria_mapa(){
	inicia_mapa();
	
	Canoa Canoa[6];
	Submarino Submarino[4];
	Porta_aviao Porta_aviao[2];
	
	system("clear");
	cout << endl << endl;
	cout << get_jogador() << " vamos montar o mapa que você atacará" << endl << "Criaremos 6 Canoas, 4 Submarinos e 2 Porta-aviões"<< endl;
	int i, x, y;
	string orientacao;
	for(i=0; i<6; i++){
		cout << "Por favor digite a linha da sua " << i+1 << "º Canoa: ";
		cin >> x; 
		cout << "Por favor digite a coluna da sua " << i+1 << "º Canoa: ";
		cin >> y;
		Canoa[i].set_x(x);
		Canoa[i].set_y(y);
	}
	for(i=0; i<6; i++){
		x=Canoa[i].get_x();
		y=Canoa[i].get_y();
		mapa[y][x]='C';
	}
	system("clear");
	
	cout << "Atenção!" << endl << "A orientação somente deve ser escrita como direita, esquerda, cima ou baixo!" << endl;
	for(i=0; i<4; i++){
		cout << "Por favor digite a linha do seu " << i+1 << "º Submarino: ";
		cin >> x; 
		cout << "Por favor digite a coluna do seu " << i+1 << "º Submarino: ";
		cin >> y;
		cout << "Por favor digite a orientação do seu " << i+1 << "º Submarino: ";
		cin >> orientacao;
		Submarino[i].set_x(x);
		Submarino[i].set_y(y);
		Submarino[i].set_orientacao(orientacao);
	}
	
	for(i=0; i<4; i++){
		y=Submarino[i].get_y();
		x=Submarino[i].get_x();
		mapa[y][x]='S';
		if(Submarino[i].get_orientacao()=="esquerda")
			mapa[y-1][x]='S';
		if(Submarino[i].get_orientacao()=="direita")
			mapa[y+1][x]='S';
		if(Submarino[i].get_orientacao()=="cima")
			mapa[y][x-1]='S';
		if(Submarino[i].get_orientacao()=="baixo")
			mapa[y][x+1]='S';		
	}
	
	system("clear");
	cout << "Atenção!" << endl << "A orientação somente deve ser escrita como direita, esquerda, cima ou baixo!" << endl;
	for(i=0; i<2; i++){
		cout << "Por favor digite a linha do seu " << i+1 << "º Porta-avião: ";
		cin >> x; 
		cout << "Por favor digite a coluna do seu " << i+1 << "º Porta-avião: ";
		cin >> y;
		cout << "Por favor digite a orientação do seu " << i+1 << "º Porta-avião: ";
		cin >> orientacao;
		Porta_aviao[i].set_x(x);
		Porta_aviao[i].set_y(y);
		Porta_aviao[i].set_orientacao(orientacao);
	}
	
	for(i=0; i<2; i++){
		x=Porta_aviao[i].get_x();
		y=Porta_aviao[i].get_y();
		mapa[y][x]='P';
		if(Porta_aviao[i].get_orientacao()=="esquerda"){
			mapa[y-1][x]='P';
			mapa[y-2][x]='P';
			}
		if(Porta_aviao[i].get_orientacao()=="direita"){
			mapa[y+1][x]='P';
			mapa[y+2][x]='P';
			}
		if(Porta_aviao[i].get_orientacao()=="cima"){
			mapa[y][x-1]='P';
			mapa[y][x-2]='P';
			}
		if(Porta_aviao[i].get_orientacao()=="baixo"){
			mapa[y][x+1]='P';
			mapa[y][x+2]='P';
			}
	}
	

}

char Jogo::transforma_visivel(char pos){
	if(pos == 'c' || pos == '0' || pos == 'p'){
		return 'X';
	}
	if(pos == 's'){
		return 's';
	}
	if(pos == 'X'){
		return 'A';
	}
	else{
		return '~';
	}
}

int Jogo::conta_pontos(){
	int y, x, pontos=0;
	for(x=0; x<13; x++){
		for(y=0; y<13; y++){
			if(mapa[y][x] == 'c' || mapa[y][x] == 'p' || mapa[y][x] == '0')
				pontos ++;
		}
	}
	return pontos;
}
