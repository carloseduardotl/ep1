#include "canoa.hpp"
#include <iostream>
#include <string>

Canoa::Canoa(int x, int y, std::string orientacao){
	set_x(x);
	set_y(y);
	set_orientacao(orientacao);
	set_destruido(false);
}

Canoa::Canoa(){
	set_orientacao("nada");
	set_destruido(false);
}
Canoa::~Canoa(){}
