#ifndef JOGO_HPP
#define JOGO_HPP

#include <iostream>
#include <string>

class Jogo{
	private:
		char mapa [13][13];
		std::string jogador;
		
	public: 
		Jogo();
		~Jogo();
		
		std::string get_jogador();
		void set_jogador(std::string jogador);
		
		void inicia_mapa();
		void imprime_mapa();
		void atualiza_mapa();
		void tiro(int x, int y);
		bool tiro_porta_aviao();
		void teste_mapa();
		void tiro_jogador();
		bool jogo_acabou();
		void cria_mapa();
		char transforma_visivel(char pos);
		int conta_pontos();

};

#endif
