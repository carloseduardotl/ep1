#ifndef PORTA_AVIAO_HPP
#define PORTA_AVIAO_HPP

#include <iostream>
#include <string>
#include "embarcacao.hpp"

class Porta_aviao : public Embarcacao{
	public:
		Porta_aviao();
		~Porta_aviao();
		Porta_aviao(int x, int y, std::string orientacao);

};

#endif
