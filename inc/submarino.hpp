#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <iostream>
#include <string>
#include "embarcacao.hpp"

class Submarino : public Embarcacao{
	private:
		int quant_ataques;
	
	public:
		Submarino();
		~Submarino();
		Submarino(int x, int y, std::string orientacao);
		
		int get_quant_ataques();
		void set_quant_ataques(int quant);
		

};

#endif
