#ifndef MENU_HPP
#define MENU_HPP

class Menu{
	private:
		int opcao;
	
	public:
		Menu();
		~Menu();

		int get_opcao();
		void set_opcao(int op);
		
		void imprime_menu();

};

#endif
