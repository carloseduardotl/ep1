#ifndef CANOA_HPP
#define CANOA_HPP

#include "embarcacao.hpp"
#include <iostream>
#include <string>

class Canoa : public Embarcacao{
	public:
		Canoa();
		~Canoa();
		Canoa(int x, int y, std::string orientacao);


};

#endif
