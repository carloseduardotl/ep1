#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>

class Embarcacao{
	private:
		int x;
		int y;
		std::string orientacao;
		bool destruido;

	public:
		Embarcacao();
		~Embarcacao();

		int get_x();
		void set_x(int x);

		int get_y();
		void set_y(int y);

		std::string get_orientacao();
		void set_orientacao(std::string orientacao);

		bool get_destruido();
		void set_destruido(bool destruido);


};



#endif
